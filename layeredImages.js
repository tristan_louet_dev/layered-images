//This is a script by robnyman at gmail dot com to provide a cross-browser
//getElementByClassName function without having to use an entire library like jQuery
var getElementsByClassName=function(e,t,n){if(document.getElementsByClassName){getElementsByClassName=function(e,t,n){n=n||document;var r=n.getElementsByClassName(e),i=t?new RegExp("\\b"+t+"\\b","i"):null,s=[],o;for(var u=0,a=r.length;u<a;u+=1){o=r[u];if(!i||i.test(o.nodeName)){s.push(o)}}return s}}else if(document.evaluate){getElementsByClassName=function(e,t,n){t=t||"*";n=n||document;var r=e.split(" "),i="",s="http://www.w3.org/1999/xhtml",o=document.documentElement.namespaceURI===s?s:null,u=[],a,f;for(var l=0,c=r.length;l<c;l+=1){i+="[contains(concat(' ', @class, ' '), ' "+r[l]+" ')]"}try{a=document.evaluate(".//"+t+i,n,o,0,null)}catch(h){a=document.evaluate(".//"+t+i,n,null,0,null)}while(f=a.iterateNext()){u.push(f)}return u}}else{getElementsByClassName=function(e,t,n){t=t||"*";n=n||document;var r=e.split(" "),i=[],s=t==="*"&&n.all?n.all:n.getElementsByTagName(t),o,u=[],a;for(var f=0,l=r.length;f<l;f+=1){i.push(new RegExp("(^|\\s)"+r[f]+"(\\s|$)"))}for(var c=0,h=s.length;c<h;c+=1){o=s[c];a=false;for(var p=0,d=i.length;p<d;p+=1){a=i[p].test(o.className);if(!a){break}}if(a){u.push(o)}}return u}}return getElementsByClassName(e,t,n)}

//This little piece of code provide an indexOf function on arrays even
//when not supported by the browser (like IE. Yeah, IE sucks)
Array.indexOf||(Array.prototype.indexOf=function(e){for(var t=0;t<this.length;t++)if(this[t]==e)return t;return-1});

//This adds a startWith method to the String protorype
String.prototype.startsWith=function(e){return 0===this.indexOf(e)};

//----------------------------------------------------------------------------//

var layeredImages = (function() {

    //Private stuff
    var debug = false;
    var moduleName = "layeredImages.js";
    var instancesArray = new Object();
    
    //utilities
    function log(level, msg) {
        console[level](moduleName + ": " + msg);
    }
    
    //Returns an string that is retrieved from a string like "layerX"
    //where X is the returned string.
    //For example, getLayerNumber("layer12"); will return 12
    function getLayerNumber(string) {
        return string.substring("layer-".length); //<-- yeah that's ugly
    }
    
    //init

    function init() {
        
        //-----------------Loading and initializing canvases------------------//
        var canvases = getElementsByClassName("layered-images-canvas");
        var total = canvases.length;
        var successNb = 0;
        
        for (var i in canvases) {
            //if a div that have the class saying it should became a layeredImages canvas
            //has no id, it is an error because each canvas is accessible through an
            //array and its id.
            if (canvases[i].id === "") {
                log("error", "no id found for a layered-images-canvas div. You must provide one !");
                continue;
            } else if (!canvases[i].id.startsWith("canvas-")) {
                log("error", "a canvas id must begins with \"canvas\" for inner implementation purpose. The id \"" + canvases[i].id + "\" does not match this rule.");
                continue;
            }
            
            // API IMPLEMENTATION
            // layeredImages can have several "instances" (canvases), so
            // we need to return the function for each one, this mean implementing
            // public API here
            var canvas = canvases[i];
            canvas["setLayer"] = function(src, layer) {
                var newImg = document.createElement("img");
                var layerClass = "layered-images-img layered-images-layer" + layer;
                newImg.setAttribute('src', src);
                newImg.setAttribute('alt', "layered image");
                newImg.setAttribute('class', layerClass);
                newImg.setAttribute('style', "z-index: " + layer + ";");
                //We remove all children of this node that have the class (i.e. layer) we
                //want to use. There should be at most 1 but let's loop over all possible
                //ones just in case.
                oldChildren = getElementsByClassName(layerClass);
                for (var j in oldChildren) {
                    canvas.removeChild(oldChildren[j]);
                }
                canvas.appendChild(newImg);
            };
            
            //Linking a canvas to its div id
            instancesArray[canvases[i].id] = canvas;
            successNb++;
        }
        
        if (debug) {
            log("debug", successNb + " canvas(es) initialized");
        }
        
        if (successNb < total) {
            log("warn", successNb + "/" + total + " canvas(es) initialized");
        }
        
        //----------------------Initializing choosers-------------------------//
        
        successNb = 0;
        total = 0;
        
        //We loop every initialized canvases to search for its corresponding
        //choosers if existant
        var choosers = getElementsByClassName("layered-images-chooser");
        //for information purpose
        total += choosers.length;

        //We loop through every chooser existing for this canvas
        for (var j in choosers) {
            var parent = choosers[j];
            var classes = parent.className.split(" ");

            //We are in the chooser container.
            //We find the canvas that this chooser is linked to

            var canvasId = "";
            //we need to loop through every classes
            //to find the one that begins with canvas-
            for (l in classes) {
                if (classes[l].startsWith("canvas-")) {
                    //We have the class that defines the right layer
                    canvasId = classes[l];
                    break; //to quit the for loop
                }
            }

            if (canvasId === "") {
                log("error", "No canvas id found into a chooser class. You must add a canvas id into a chooser class to link the two of them.");
                continue;
            }
            //We have the canvas id, we must find an existing canvas
            //with that id
            var canvas = instancesArray[canvasId];

            //We also find the layer that this chooser will act onto
            var layer = -1;

            //we need to loop through every classes
            //to find the one that begins with layer
            for (l in classes) {
                if (classes[l].startsWith("layer-")) {
                    //We have the class that defines the right layer
                    layer = getLayerNumber(classes[l]);
                    break; //to quit the for loop
                }
            }

            if (layer === -1) {
                log("error", "No valid layer class found. You must add a class that looks like 'layer2' to tell the chooser which layer its images belongs to.");
                continue;
            }
            
            //We find all direct img children and we implement
            //an onclick function that will put the
            //image into the canvas and at the right layer
            var children = parent.children;
            for (var k in children) {
                if (children[k].tagName === "IMG") {
                    //This is an ugly fix to trick JS asynchronous onClick call. This is needed
                    //because when the user will click, canvas and layer variables may have change while loops continue.
                    //By doing this, we save on each loop the state of the current variables.
                    children[k].onclick = function(canvas, layer) {
                        return function() {
                            canvas.setLayer(this.src, layer);
                        }
                    }(canvas, layer);
                }
            }
            successNb++;
        }
        
        if (debug) {
            log("debug", successNb + " chooser(s) initialized");
        }
        
        if (successNb < total) {
            log("warn", successNb + "/" + total + " chooser(s) initialized");
        }
    }

//-------------- Initialization ------------------//
    function addListener(event, func) {
        if (window.addEventListener) {
            window.addEventListener(event, func, false);
        } else if (document.addEventListener) {
            document.addEventListener(event, func, false);
        } else if (window.attachEvent) {
            window.attachEvent("on" + event, func);
        }
    }

    if (document.getElementById && document.createTextNode) {
        addListener("load", init); //init() will be called after total page loading
    }
//------------------------------------------------//

    //Public stuff
    return {
        "canvases": instancesArray
    };
})();
