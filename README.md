# Layered Images README

Layered Images is a Javascript library that will use a special div in the HTML
DOM to display layered images. Those images will be displayed one over the
others, depending of their layer number to know which is upper the other one (using z-indexes).
So, while using transparent images, the user will have the illusion of only
one image, that will be created from the layering of several.

